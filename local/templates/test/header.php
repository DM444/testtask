<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<!doctype html>
<html class="no-js" lang="">
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle()?></title>

    <!-- Place favicon.ico in the root directory -->
<?
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/css/normalize.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/css/font-awesome.min.css");

    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/css/fonts.css");

    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/css/remodal.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/css/jquery-confirm.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/slick/slick.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/fancybox/jquery.fancybox.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/fancybox/helpers/jquery.fancybox-thumbs.css");

    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/dist/css/main.css");

?>
    <script src="<?=SITE_TEMPLATE_PATH?>/dist/js/vendor/modernizr-2.8.3.min.js"></script>
    
</head>
<body class="wrap">
<?$APPLICATION->ShowPanel();?>
<header class="header">
    <div class="header__main__wrap">
        <div class="container header__container">
            <div class="header__logo">
                <div class="header__logo__link">
                    <div class="header__logo__image">
                        <img src="<?=SITE_TEMPLATE_PATH?>/content/logo.png"/>
                    </div>
                </div>
                <div class="header__logo__text">
                    Установка натяжных потолков и осветительных приборов,<br/>поставка и монтаж светодиодного
                    оборудования
                </div>
            </div>

            <div class="header__contacts__wrap clearfix">
                <a href="callto:8(383)000-00-00" class="header__contacts__phone">8 (383) 000-00-00</a>
                <a href="#" data-remodal-target="call" class="button button__small">
                    <span class="button__text">Заказать звонок</span>
                </a>

            </div>
        </div>
    </div>
</header>

<?$APPLICATION->IncludeComponent(
    "bitrix:menu",
    "template1",
    Array(
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "left",
        "DELAY" => "N",
        "MAX_LEVEL" => "1",
        "MENU_CACHE_GET_VARS" => array(0=>"",),
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "ROOT_MENU_TYPE" => "top",
        "USE_EXT" => "Y"
    )
);?>