<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<div class="page__menu__fixed">
    <nav class="page__menu">
        <div class="container">
            <ul class="menu__list">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<li class="menu__item">
        <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
    </li>
<?endforeach?>

			</ul>
        </div>
    </nav>
</div>

<?endif?>