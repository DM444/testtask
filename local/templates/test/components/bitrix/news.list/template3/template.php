<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="reviews__slider__block">
    <div class="reviews__slider js__reviews">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	    <div class="review__slide">
	        <div class="review__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	            <div class="review__image review__image_left_top">
	                <a href="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="fancybox" rel="reviews">
	                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"/>
	                </a>
	            </div>
	            <div class="review__image review__image_right_bottom">
	                <a href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" class="fancybox" rel="reviews">
	                    <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"/>
	                </a>
	            </div>
	        </div>
	    </div>
                 
<?endforeach;?>

    </div>
</div>


